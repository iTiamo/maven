package tiamo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tiamo.model.Film;
import tiamo.service.FilmService;

import java.util.List;

@RestController
public class FilmController {
    private FilmService service;

    public FilmController(FilmService service) {
        this.service = service;
    }

    @RequestMapping("/films")
    public List<Film> getAllFilms() {
        return service.getAllFilms();
    }
}
