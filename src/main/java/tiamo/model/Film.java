package tiamo.model;

public class Film {
    private long id;
    private String director;
    private String title;
    private int year;

    public Film(long id, String director, String title, int year) {
        this.id = id;
        this.director = director;
        this.title = title;
        this.year = year;
    }

    public long getId() {
        return id;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id='" + id + '\'' +
                ", director='" + director + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                '}';
    }
}
