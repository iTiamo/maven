package tiamo.service;

import org.springframework.stereotype.Service;
import tiamo.model.Film;

import java.util.Arrays;
import java.util.List;

@Service
public class FilmService {
    private List<Film> films = Arrays.asList(
            new Film(0, "Quentin Tarantino", "Kill Bill", 2003),
            new Film(1, "Christopher Nolan", "Interstellar", 2014),
            new Film(2, "Steven Spielberg", "Saving Private Ryan", 1998)
    );

    public List<Film> getAllFilms() {
        return films;
    }

    public Film getFilmById(long id) {
        for (Film f : films) {
            if (f.getId() == id) {
                return f;
            }
        }

        return null;
    }

    public void addFilm(Film f) {
        films.add(f);
    }
}
